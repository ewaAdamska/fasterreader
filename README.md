# Faster2TTree

## How to run it

1). Compilation & running

```bash
$ cd Faster2TTree
$ cmake && make
$ ./fasterReader_2TTree path_to_faster_file
```
2). Results (as TTree named DataTree) will be written to the ```output.root``` file

## How to read it

All root macros need changing name of the readed file in the source code, and can be run by
```bash
$ root -l root_macro_name
```
It is also possible to change logical conditions applied to the histograms.


1). Reading and comparing specific spectra from ```output.root``` is provided in the ```Faster2TTree/TTrees/TTree_compare_histograms.C```.

2). 2D histogram (energy vs. detector id) of data from ```output.root``` can be found in the```Faster2TTree/TTrees/TTree_2TTree_2D_histogram.C```.

3). Single histogram can be obtained with the ```Faster2TTree/TTrees/TTree_simple_read.C``` macro.

4). All detectors spectra catalogue can be done by macro ```Faster2TTree/TTrees/TTree_spectra_catalogue.C```. It writtes catalogue to the .pdf file.





# Faster2hdf5

in progress
