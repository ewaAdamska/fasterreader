int TTree_spectra_catalogue () {

  // using namespace std;

  UInt_t                  id;
  Int_t                   energy;
  UInt_t                  pileup;
  UInt_t                  saturated;
  Double_t                timestamp;

  TFile *f = new TFile("ttree_run_19_0001.root");
  TTree *t = (TTree*) f->Get ("DataTree");

  t->SetBranchAddress ("id" , &id  );
  t->SetBranchAddress ("energy" , &energy  );
  t->SetBranchAddress ("saturated" , &saturated  );
  t->SetBranchAddress ("timestamp" , &timestamp  );

  TCanvas *c1 = new TCanvas("c1","c1",1024,800);
  gStyle->SetOptStat(0);
  gStyle->SetLegendTextSize(0.04);



  for (int det_id=1; det_id!=171; det_id++)
  {
    TH1F *hist1 = new TH1F("hist1", "", 516, 100, 70000);
    TH1F *hist2 = new TH1F("hist2", "", 516, 100, 70000);

    TString string_test = Form("id==%d", det_id);

    TCut cut1 (string_test) ;
    TCut with_pileups ("pileup!=100") ;
    TCut with_NO_pileups ("pileup==0") ;
    TCut with_NO_saturation ("saturated==0") ;

    t->Project("hist1", "energy", (cut1&&with_pileups));
    t->Project("hist2", "energy", (cut1&&with_NO_pileups));

    Float_t entries_1;
    Float_t entries_2;
    Float_t ratio;

    entries_1 = hist1->GetEntries();
    entries_2 = hist2->GetEntries();

    ratio = 1.- (entries_2/entries_1);
    cout << "entries_1  " << entries_1 <<" entries_2  "<< entries_2 << " ratio " << ratio << endl;


    char legend_text[20];
    char legend_with_pilups[40];
    sprintf(legend_text, "id = %d", det_id);
    sprintf(legend_with_pilups, "pileups (ratio %f)", ratio);


    hist1->Draw();
    hist2->SetLineColor(kRed);
    hist2->Draw("same");

    TLegend *legend = new TLegend(0.6, 0.88,0.89,0.7);
    legend->SetHeader(legend_text, "C");
    legend->AddEntry(hist1, legend_with_pilups, "l");
    legend->AddEntry(hist2, "no pileups", "l");
    legend->Draw();



    char outputFileName[20];
    sprintf(outputFileName, "Spectra_catalogue/%d.png", det_id);
    c1->SaveAs(outputFileName);


    TString title = Form("Title:Det %d", det_id);

    if (det_id==1)  {
       c1->Print("Spectra_catalogue/plots.pdf(", title);
    }else if (det_id==170){
       c1->Print("Spectra_catalogue/plots.pdf)", "spectra from all dets");
    } else {
      c1->Print("Spectra_catalogue/plots.pdf", title);
    }

    c1->Clear();

    delete hist1;
    delete hist2;
  };




  return 0;
}
