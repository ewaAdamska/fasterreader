/*
* Faster to HDF5 reader
*/

//  std includes
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>

// //  root includes
// #include "TFile.h"
// #include "TTree.h"

//  fasterac includes
#include "fasterac/fasterac.h"
#include "fasterac/fast_data.h"
#include "fasterac/qdc_caras.h"
#include "fasterac/utils.h"
#include "fasterac/adc_caras.h"
#include "fasterac/rf_caras.h"

// hdf5 includes
#include "H5Cpp.h"
using namespace H5;
using namespace std;



const H5std_string DATASET_NAME( "Dataset" );
const H5std_string MEMBER1( "id" );
const H5std_string MEMBER2( "energy" );
const H5std_string MEMBER3( "energy2" );
const H5std_string MEMBER4( "time");
const H5std_string MEMBER5( "pileup" );
const H5std_string MEMBER6( "saturated" );

const int   BUFFER_LENGTH = 10000;
const int   RANK = 1;
const int   COMPRESSION_FACTOR = 0;
/*
* Preprare dataset extending
*/

hsize_t size[1];
hsize_t dims[1];

hsize_t offset[1] = {BUFFER_LENGTH};
hsize_t dimsext[1] = {BUFFER_LENGTH};



/*
 * Initialize the data buffer
 */
 typedef struct record_structure {
  int                  id;
  int                energy;
  int                energy2;
  uint64_t               time;
  bool                  pileup;
  bool                  saturated;
} record_structure;

record_structure buffer[BUFFER_LENGTH];


void buffer_writting_to_h5file(     int   &i,
                                  DataSet* &dataset,
                                hsize_t &extend_numb,
                                CompType &mtype1)
{

   //Extend the dataset

  offset[0]=(extend_numb-1)*BUFFER_LENGTH;
  size[0] = {extend_numb*BUFFER_LENGTH};

  dataset->extend(size);
  extend_numb++;


  // Select a hyperslab in extended portion of the dataset.
  DataSpace *filespace = new DataSpace(dataset->getSpace ());
  filespace->selectHyperslab(H5S_SELECT_SET, dimsext, offset);

  // Define memory space.
  DataSpace *memspace = new DataSpace(1, dimsext, NULL);

  // Write data to the extended portion of the dataset.
  dataset->write(buffer, mtype1, *memspace, *filespace);

  // Close all objects and file.
  // prop.close();
  delete filespace;
  delete memspace;

  // reseting buffer iteration index
  i=0;
}


void buffer_clearing()
{
  for (int i_buff=0; i_buff<BUFFER_LENGTH; i_buff++)
    {
      buffer[i_buff].id = 0;
      buffer[i_buff].energy = 0;
      buffer[i_buff].energy2 = -1;
      buffer[i_buff].time = 0;
      buffer[i_buff].pileup = false;
      buffer[i_buff].saturated = false;
    }
}
int buffer_last_clearing()
{
  int k=0;
  for (int i_buff=0; i_buff<BUFFER_LENGTH; i_buff++)
    {
      if(buffer[i_buff].energy == 0 && buffer[i_buff].id == 0 &&
      buffer[i_buff].energy2 == -1)
      {
      k++;
      }

      buffer[i_buff].id = 0;
      buffer[i_buff].energy = 0;
      buffer[i_buff].energy2 = -1;
      buffer[i_buff].time = 0;
      buffer[i_buff].pileup = false;
      buffer[i_buff].saturated = false;
    }
    return k;
}

void buffer_testing()
  {  printf("\n------------------------------\n");
     for (int j=0; j<BUFFER_LENGTH; j++)
     {
       cout<< "i=" << j << " \tid " << buffer[j].id << "\t\tenergy  " << buffer[j].energy;
       cout<< "\t pileup "<<buffer[j].pileup <<"\t time\t"<<buffer[j].time << endl;
     }
}

bool is_buffer_full(int i)
{if (i==BUFFER_LENGTH) return true;
 else return false;

}

void record_reader (        int       &buffer_row_number,
                            faster_data_p      data,
                            unsigned long long ref_ns) {


  //  faster data  (fasterac.h)
  unsigned char          alias;
  unsigned short         label;
  unsigned short         lsize;


  //  rf           (rf.h)
  rf_data                rf;
  rf_counter             rf_cnt;
  //  qdc          (qdc.h)
  qdc_t_x1               LABR_qdc;
  qdc_t_x2               EDEN_qdc;

  //  spectro      (spectro.h)
  crrc4_spectro          crrc4_sp;
  trapez_spectro         trapez_sp;
  //  group        (fast_data.h)
  faster_buffer_reader_p group_reader;
  void*                  group_buffer;
  faster_data_p          group_data;
  int                    group_n = 0;

  //  unlock
  unlock                 dlock;
  //
  char                   clk_str [256];

  uint64_t clock_g;


  //
  alias     = faster_data_type_alias (data);
  label     = faster_data_label      (data);
  lsize     = faster_data_load_size  (data);


  //-----------
  switch (alias) {
    case QDC_TDC_X1_TYPE_ALIAS:
      faster_data_load (data, &LABR_qdc);

      if (LABR_qdc.q1>100){
        clock_g = faster_data_hr_clock_ns(data)*1000;

        buffer[buffer_row_number].energy = LABR_qdc.q1;
        buffer[buffer_row_number].energy2 = -1;
        buffer[buffer_row_number].id = label;
        buffer[buffer_row_number].time = clock_g;
        buffer[buffer_row_number].pileup = false;

        if(LABR_qdc.q1_saturated) buffer[buffer_row_number].saturated = true;
        else buffer[buffer_row_number].saturated = false;

        buffer_row_number++;
      }
      break;
    case QDC_TDC_X2_TYPE_ALIAS:
      faster_data_load (data, &EDEN_qdc);

      if (EDEN_qdc.q1>100){
        clock_g = faster_data_hr_clock_ns(data)*1000;

        buffer[buffer_row_number].energy = EDEN_qdc.q1;
        buffer[buffer_row_number].energy2 = EDEN_qdc.q2;
        buffer[buffer_row_number].id = label;
        buffer[buffer_row_number].time = clock_g;
        buffer[buffer_row_number].pileup = false;

        if(EDEN_qdc.q1_saturated) buffer[buffer_row_number].saturated = true;
        else buffer[buffer_row_number].saturated = false;

        buffer_row_number++;
      }
      break;
    case CRRC4_SPECTRO_TYPE_ALIAS:
      faster_data_load (data, &crrc4_sp);
      if (crrc4_sp.measure>100)
      {
        clock_g = faster_data_hr_clock_ns(data)*1000;
        buffer[buffer_row_number].energy = crrc4_sp.measure;
        buffer[buffer_row_number].energy2 = -1;
        buffer[buffer_row_number].id = label;
        buffer[buffer_row_number].time = clock_g;

        if(crrc4_sp.pileup) buffer[buffer_row_number].pileup = true;
        else buffer[buffer_row_number].pileup = false;

        if(crrc4_sp.saturated) buffer[buffer_row_number].saturated = true;
        else buffer[buffer_row_number].saturated = false;

        buffer_row_number++;
      }
      break;

    case TRAPEZ_SPECTRO_TYPE_ALIAS:
      faster_data_load (data, &trapez_sp);
      if (trapez_sp.measure>100)
      {
        // hr_clock =  (long double)clock + trapez_spectro_conv_dt_ns (trapez_sp.tdc);   // long double

        clock_g = faster_data_hr_clock_ns(data)*1000;

      buffer[buffer_row_number].energy = trapez_sp.measure;
      buffer[buffer_row_number].energy2 = -1;
      buffer[buffer_row_number].id = label;
      buffer[buffer_row_number].time = clock_g;

      if(trapez_sp.pileup) buffer[buffer_row_number].pileup = true;
      else buffer[buffer_row_number].pileup = false;

      if(trapez_sp.saturated) buffer[buffer_row_number].saturated = true;
      else buffer[buffer_row_number].saturated = false;

      buffer_row_number++;
      }
      break;
    default:
      printf ("TYPE NOT RECOGNISED %15s %5d  %s", type_name (alias), label, clk_str);
      printf ("\n");
  }



  }

int main (int argc, char** argv) {

  /************/
  /*  FASTER  */
  /************/
  //  file reader
  faster_file_reader_p   reader;
  //  data
  faster_data_p          data;
  unsigned char          alias;
  unsigned char          alias_in_group;
  unsigned short         label;
  //  group data
  faster_buffer_reader_p group_reader;
  char                   group_buffer [1500];
  unsigned short         lsize;
  faster_data_p          group_data;
  //  qdc tdc data  (from faster group)
  qdc_t_x1               qdc1;
  qdc_t_x1               qdc3;



  char* DATAFILENAME = argv[1];

  string h5FILENAME(DATAFILENAME);
  h5FILENAME.erase(h5FILENAME.end()-5, h5FILENAME.end());
  h5FILENAME=h5FILENAME+"_compressed_"+std::to_string(COMPRESSION_FACTOR)+".h5";

  //  print infos
  printf ("     - read faster file '%s'\n", DATAFILENAME);
  printf ("     - save as %s \n", h5FILENAME.c_str());

  //  open faster file reader
  reader = faster_file_reader_open (DATAFILENAME);
  if (reader == NULL) {
    printf ("error opening file %s\n", DATAFILENAME);
    return EXIT_FAILURE;
  }


  /*
   * Create the hdf5 data space.
   */

  hsize_t dim[] = {BUFFER_LENGTH};   /* Dataspace dimensions */
  hsize_t maxdim[] = {H5S_UNLIMITED};   /* Dataspace dimensions */
  hsize_t chunk_dims[1] ={BUFFER_LENGTH};

  DataSpace space( RANK, dim, maxdim );

  /*
  * Modify dataset creation property to enable chunking
  */



  DSetCreatPropList prop;
	prop.setChunk(1, chunk_dims);
  // prop.setDeflate(COMPRESSION_FACTOR); //compressiontest

  size[0]={0};



  /*
   * Create the file.
   */
  H5File* file = new H5File( h5FILENAME, H5F_ACC_TRUNC );

  /*
   * Create the memory datatype.
   */
  CompType mtype1( sizeof(record_structure) );
  mtype1.insertMember( MEMBER1, HOFFSET(record_structure, id), PredType::NATIVE_USHORT);
  mtype1.insertMember( MEMBER2, HOFFSET(record_structure, energy), PredType::NATIVE_INT);
  mtype1.insertMember( MEMBER3, HOFFSET(record_structure, energy2), PredType::NATIVE_INT);
  mtype1.insertMember( MEMBER4, HOFFSET(record_structure, time), PredType::NATIVE_ULONG);
  mtype1.insertMember( MEMBER5, HOFFSET(record_structure, pileup), PredType::NATIVE_HBOOL);
  mtype1.insertMember( MEMBER6, HOFFSET(record_structure, saturated), PredType::NATIVE_HBOOL);

  /*
   * Create the dataset.
   */
  DataSet* dataset = new DataSet(file->createDataSet(DATASET_NAME, mtype1, space, prop));


  // buffer iteration i:
  int  i=0;
  // dataset extension number
  hsize_t extend_numb = 1;

  /*
  * main loop
  */
  int k=0;
  while ((data = faster_file_reader_next (reader)) != NULL) {                    //  read each data
    alias = faster_data_type_alias (data);
    if (alias == GROUP_TYPE_ALIAS) {
      lsize = faster_data_load (data, group_buffer);                             //  get group data
      group_reader = faster_buffer_reader_open (group_buffer, lsize);            //  open group reader
      while ((group_data = faster_buffer_reader_next (group_reader)) != NULL)  //  read data inside the group
      {
        label = faster_data_label (group_data);

        record_reader(i, group_data, 0);

        if (i==BUFFER_LENGTH)
        {
          buffer_writting_to_h5file(i, dataset, extend_numb, mtype1);
          // buffer_testing();
          buffer_clearing();
        }


        //printf("\ngroup row readed\n"); // test
        //printf("%s %s\n", type_name(alias),type_name(alias_in_group));
      }
      faster_buffer_reader_close (group_reader);

    }// end of if alias=group
    else{// ALL TYPES DIFFERENT THAN GROUP
      record_reader(i, data, 0);
      if (i==BUFFER_LENGTH)
      {
        buffer_writting_to_h5file(i, dataset, extend_numb, mtype1);
        // buffer_testing();
        buffer_clearing();
      }}


  }// end of the while loop
  if (i!=BUFFER_LENGTH)
  {
    buffer_writting_to_h5file(i, dataset, extend_numb, mtype1);
    // buffer_testing();
    k = buffer_last_clearing();
  }
  cout<<"\nNUMBER OF 0-ROWS\t"<<k<<endl;

  /*
   * Release resources
   */
  delete dataset;
  delete file;


  //  close files & quit
  faster_file_reader_close (reader);
  return EXIT_SUCCESS;
}
